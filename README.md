Dieses Repository enthält Jupyter Notebooks, die zur OER "Faires Machine Learning" 
(veröffentlicht unter https://iug.htw-berlin.de/projekte/fair-enough/) gehören.

Der Ordner "IdentiTOP" enthält das Notebook, das einen Audit für den in der OER
beschriebenen Use Case "IdentiTOP" demonstriert. Außerdem ist der für den Audit
verwendete synthetische Datensatz enthalten und das Notebook, mit dem dieser Datensatz generiert wurde.

Der Ordner "Kreditwuerdigkeitsmodell" enthält ein Notebook, mit dem Lernende einen Audit für ein Kreditwürdigkeitsmodell durchführen können (als Aufgabe).
Außerdem ist eine Lösung für den Audit in einem weiteren Notebook gegeben. Auch der Datensatz, auf dem der Audit durchgeführt wird, befindet sich in diesem Ordner, sowie
das Notebook, mit dem dieser Datensatz synthetisiert wurde.
