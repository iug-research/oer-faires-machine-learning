{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "## Über dieses Notebook\n",
    "Banken wollen Kredite nur an Personen vergeben, die auch kreditwürdig sind. Dafür nutzt eine fiktive Bank ein Machine Learning Modell, das einschätzt, ob eine Person kreditwürdig ist. Die Bank hat uns beauftragt, zu prüfen, ob ihr Modell fair ist. Dieses Notebook beinhaltet die Lösung für diese Aufgabe.\n",
    "\n",
    "Dieses Notebook ist wie folgt aufgebaut: \n",
    "1. Identifizierung möglicher Risiken des Kreditwürdigkeitsmodells\n",
    "2. Festlegung von Qualitätsmetriken zur Überprüfung der Risiken des Kreditwürdigkeitsmodells\n",
    "3. Berechnung der Qualitätsmetriken für das Kreditwürdigkeitsmodell\n",
    "4. Interpretation der Ergebnisse: Welche Risiken hat das Kreditwürdigkeitsmodell wirklich? Wie könnte das Kreditwürdigkeitsmodell verbessert werden?\n",
    "\n",
    "### Verwendete Bibliotheken\n",
    "* Dieses Notebook verwendet zur Datenverarbeitung die Bibliothek [pandas](https://pandas.pydata.org/docs/reference/index.html).  \n",
    "* Die Bibliothek [sklearn](https://scikit-learn.org) wird verwendet um [Qualitätsmetriken](https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics) zu berechnen.\n",
    "* Die Bibliothek [fairlearn](https://fairlearn.org/v0.8/user_guide/) bietet die Möglichkeit, Qualitäts-Metriken pro Gruppe für das Modell berechnen zu lassen. Diese Bibliothek muss ggfs. zuerst installiert werden, z.B. via `pip install fairlearn` oder Anaconda."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 1. Identifizierung möglicher Risiken\n",
    "\n",
    "Reminder: In unserem Datensatz sind die folgenden demographischen Daten verfügbar:\n",
    "* \"dependents\" - Ist die Person für Kinder verantwortlich? (Kodierung: 0 = hat keine Kinder, 1 = hat Kinder)\n",
    "* \"female\" - Ist die Person weiblich? (Kodierung: 0 = männlich, 1 = weiblich)\n",
    "\n",
    "Allgemeine Risiken\n",
    "\n",
    "* (R1) Eine nicht-kreditwürdige Person erhält einen Kredit, den sie schwerlich oder nicht zurückzahlen kann. Das führt zu einer Beeinträchtigung der gefühlten Freiheit der Person, da sie das Abzahlen des Kredits vor andere Ziele stellen muss, wenn sie den Kredit doch noch bedienen können möchte. Bei Verzögerter Bedienung des Kredits oder wenn die Person es nicht schafft, den Kredit abzuzahlen, macht die Bank außerdem finanzielle Verluste.\n",
    "* (R2) Eine kreditwürdige Person erhält keinen Kredit, obwohl sie ihn bezahlen könnte. Das führt dazu, dass die Person ihre Wünsche (z.B. eine eigene Firma zu gründen, ein Haus zu kaufen) schwerer verwirklichen kann. Der Bank entgeht außerdem ein Gewinn.\n",
    "\n",
    "Fairness-Risiken\n",
    "\n",
    "* (R1 a) Personen mit Kindern könnten mehr Schwierigkeiten haben, einen Kredit zu bedienen. In diesem Fall wäre es besonders gravierend, wenn sie fälschlicherweise als kreditwürdig eingestuft werden, da sich das auch negativ auf die Angehörigen auswirkt. \n",
    "* (R2 a) Erhält eine Person mit Kindern einen Kredit fälschlicherweise nicht, so ist das schlimmer als bei Personen ohne Kinder, da sich der fehlende Kredit auch auf die Zukunftschancen der Kinder auswirken könnte.\n",
    "* (R2 b) In der Vergangenheit war es unüblich, dass Frauen finanziell eigenständig waren. Es könnte daher sein, dass mangels aktueller Informationen das Modell gelernt hat, Frauen Kredite zu verwehren, obwohl sie diese eigentlich bedienen könnten. Für die Bank gibt es hier auch ein rechtliches Risiko, da die Diskriminierung nach Geschlecht verboten ist."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 2. Festlegung von Qualitätsmetriken zur Überprüfung der Risiken\n",
    "* (R1) Das Risiko ist akzeptabel, wenn von allen nicht-kreditwürdigen Personen (TN + FP) nur wenige Personen falsch klassifiziert (FP) werden. Daher ist die \"False positive rate\" zu berechnen: FPR = FP / (TN + FP)\n",
    "* (R2) Das Risiko ist akzeptabel, wenn von allen kreditwürdigen Personen (TP + FN) nur wenige Personen falsch klassifiziert (FN) werden. Daher ist die \"False negative rate\" zu berechnen: FNR = FN / (TP + FN)\n",
    "* (R1 a) Predictive Equality (gleiche FPR) für Personen mit und ohne Kinden\n",
    "* (R2 a) Equal Opportunity (gleiche FNR) für Personen mit und ohne Kindern\n",
    "* (R2 b) Equal Opportunity (gleiche FNR) für Frauen wie für Männer\n",
    "\n",
    "Hinweise: \n",
    "* FP und TN sind nur Buchstabensalat für dich? Wiederhole die [Confusion matrix](https://en.wikipedia.org/wiki/Confusion_matrix)!\n",
    "* Viele Fairness-Definitionen (z.B. Predictive Equality, Equal Opportunity) werden von [Verma & Rubin 2018](https://fairware.cs.umass.edu/papers/Verma.pdf) erklärt."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 3. Berechnung der Qualitätsmetriken\n",
    "\n",
    "Für die Berechnung der Qualität des Kreditwürdigkeitsmodells hat uns die Bank einen Datensatz zur Verfügung gestellt mit demographischen Daten von Kund:innen (Spalten \"female\" und \"dependents\"), Kreditwürdigkeitsvorhersagen des Modells (Spalte \"prediction\"), sowie ob der:die Kund:in wirklich kreditwürdig ist oder nicht (Spalte \"truth\").\n",
    "\n",
    "Die Daten sind wie folgt kodiert:\n",
    "* Spalte \"female\" = 1 wenn die Person weiblich ist, ansonsten 0\n",
    "* Spalte \"dependents\" = 1 wenn die Person Kinder hat, ansonsten 0\n",
    "* Spalte \"prediction\" = 1 wenn das Modell vorhergesagt hat, dass die Person kreditwürdig ist, ansonsten 0\n",
    "* Spalte \"truth\" = 1 wenn die Person in Wirklichkeit kreditwürdig ist, ansonsten 0\n",
    "\n",
    "Im ersten Schritt werden die Daten importiert."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "d = pd.read_csv(\"credit-prediction-results.csv\") # import the CSV\n",
    "\n",
    "d.head() # show a few rows of the imported data set"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### (R1) und (R2) - Allgemeine Risiken\n",
    "\n",
    "Für (R1) und (R2) wird zunächst die [Confusion Matrix](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html#sklearn.metrics.confusion_matrix) berechnet und visualisiert. "
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay\n",
    "\n",
    "tn, fp, fn, tp = confusion_matrix(d[\"truth\"], d[\"prediction\"]).ravel() # Get the confusion matrix.\n",
    "ConfusionMatrixDisplay.from_predictions(d[\"truth\"], d[\"prediction\"], display_labels=['not creditable', 'creditable'], cmap='binary')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Aufbauend auf der Confusion Matrix werden die Werte für TN, FP usw. in die Formeln für FPR und FNR eingesetzt und so die Metriken berechnet."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "fpr = fp / (tn + fp) # Calculate FPR\n",
    "print('(R1) FPR: {}'.format(fpr))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "fnr = fn / (tp + fn) # Calculate FNR\n",
    "print('(R2) FNR: {}'.format(fnr))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### (R1a), (R2a) und (R2b) - Qualität pro Gruppe / Diskriminierungsrisiken\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from fairlearn.metrics import MetricFrame, count, false_negative_rate, false_positive_rate\n",
    "\n",
    "a = MetricFrame(metrics={\n",
    "    \"FPR\": false_positive_rate,  # for predictive equality - R1 a\n",
    "    \"FNR\": false_negative_rate},  # for equal opportunity - R2 a\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[\"dependents\"]).by_group"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Die Gleichheit der Qualität pro Gruppe kann durch die Ratio oder die Differenz bestimmt werden.\n",
    "\n",
    "Wie gleich muss die Qualität sein? Dies ist nicht festgelegt. Die Stakeholder müssen entscheiden, ob der Unterschied zu groß ist. Sie können sich z.B. an der weit verbreiteten \"[Disparate Impact](https://en.wikipedia.org/wiki/Disparate_impact)\"- oder \"80%\"-Regel orientieren, nach der die Ratio > 0.8 und <1.2 sein soll, bzw. die Differenz < 0.2 sein soll."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R1 a\n",
    "print(\"Predictive Equality for people with children\")\n",
    "print(\"\\n\")\n",
    "print(a[\"FPR\"])\n",
    "\n",
    "fpr_nokids = a[\"FPR\"][0]\n",
    "fpr_kids = a[\"FPR\"][1]\n",
    "\n",
    "print(\"\\n\")\n",
    "print(\"FPR-Ratio: {} / {} = {}\".format(fpr_nokids, fpr_kids, fpr_nokids/fpr_kids))\n",
    "print(\"FPR-Difference: {} - {} = {}\".format(fpr_nokids, fpr_kids, fpr_nokids - fpr_kids))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R2 a\n",
    "print(\"Equal Opportunity for people with children\")\n",
    "print(\"\\n\")\n",
    "print(a[\"FNR\"])\n",
    "\n",
    "fnr_nokids = a[\"FNR\"][0]\n",
    "fnr_kids = a[\"FNR\"][1]\n",
    "\n",
    "print(\"\\n\")\n",
    "print(\"FNR-Ratio: {} / {} = {}\".format(fnr_nokids, fnr_kids, fnr_nokids/fnr_kids))\n",
    "print(\"FNR-Difference: {} - {} = {}\".format(fnr_nokids, fnr_kids, fnr_nokids - fnr_kids))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R2 b\n",
    "print(\"Equal Opportunity for Women\")\n",
    "print(\"\\n\")\n",
    "b = MetricFrame(metrics={\"FNR\": false_negative_rate},  # for equal opportunity - R2 b\n",
    "                y_true=d[\"truth\"],\n",
    "                y_pred=d[\"prediction\"],\n",
    "                sensitive_features=d[\"female\"]).by_group\n",
    "print(b)\n",
    "\n",
    "fnr_mas = b[\"FNR\"][0]\n",
    "fnr_fem = b[\"FNR\"][1]\n",
    "\n",
    "print(\"\\n\")\n",
    "print(\"{} / {} = {}\".format(fnr_fem, fnr_mas, fnr_fem/fnr_mas))\n",
    "print(\"{} - {} = {}\".format(fnr_fem, fnr_mas, fnr_fem - fnr_mas))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "#### Intersektionale Fairness\n",
    "In der Fairness-Forschung hat man herausgefunden, dass Diskriminierung sich oft \"aufaddiert\". D.h. wenn man beobachten kann dass z.B. People of Color diskriminiert werden und Frauen diskriminiert werden, dass dann Women of Color besonders stark diskriminiert werden. \"Women of Color\" ist eine intersektionale Guppe. Es muss daher auch geprüft werden, wie gut das Modell für die verschiedenen intersektionalen Gruppen funktioniert."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "c = MetricFrame(metrics={\n",
    "    \"FPR\": false_positive_rate,  # for predictive equality\n",
    "    \"FNR\": false_negative_rate,  # for equal opportunity\n",
    "    \"count\": count }, # additional info that could be interesting\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[[\"dependents\", \"female\"]]).by_group\n",
    "\n",
    "print(c)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 4. Interpretation der Ergebnisse\n",
    "a) Interpretiere die Messungen für jedes Risiko und ziehe ein Fazit über die Stärken und Schwächen des Modells.\n",
    "\n",
    "Die Wahrscheinlichkeit, dass eine Person einen Kredit erhält, den sie nicht bedienen kann, liegt bei ~6% (R1). Die Wahrscheinlichkeit, dass einer kreditwürdigen Person ein Kredit verwehrt wird, liegt bei ~13% (R2). Das bedeutet, dass einige Personen durch einen Kredit bei dieser Bank ihre Wünsche nicht erfüllen können und wesentlich weniger Personen durch einen nicht-bedienbaren Kredit in Not geraten. \n",
    "\n",
    "Die Wahrscheinlichkeit, einen Kredit fälschlicherweise zu erhalten, ist für kinderlose Personen etwa gleich hoch wie für Eltern (R1 a). Die Betrachtung nach Gechlecht zeigt jedoch, dass Mütter nie ein Kredit erhalten, den sie nicht bedienen können und kinderlose Frauen nur in ~3% der Fälle. Männer hingegen erhalten in 9-10% der Fälle fälschlicherweise einen Kredit, unabhängig davon, ob sie Kinder haben. Beantragt ein Vater also einen Kredit bedeutet das ein höheres Risiko für ihn und seine Familie, als wie wenn eine Mutter einen Kredit beantragt. Die Bank hat bei Krediten für Männer ein höheres Verlustrisiko als bei Krediten für Frauen. \n",
    "\n",
    "Die Wahrscheinlichkeit, dass fälschlicherweise ein Kredit verwehrt wird, ist für kinderlose Personen nur etwas mehr als halb so groß wie für Eltern (R2 a). Die Wahrscheinlichkeit, dass Frauen fälschlicherweise ein Kredit verwehrt wird, ist doppelt so hoch wie bei Männern (bzw. 8 Prozentpunkte höher) (R2 b). Die intersektionale Betrachtung ergänzt dieses Bild: Das Risiko, dass kinderlose Frauen und Väter fälschlicherweise ein Kredit verwehrt wird, liegt bei beiden Gruppen lediglich bei 3-4%. Für kinderlose Männer liegt die Wahrscheinlichkeit bei 13% und für Mütter bei 23%. Während kinderlose Frauen und Väter also etwa gleichgestellt sind, liegt eine Diskriminierung nach Geschlecht und Elternschaft vor - es werden sowohl Mütter als auch kinderlose Männer diskriminiert. Die Diskriminierung ist bei Müttern am stärksten ausgeprägt - sie erhalten am wenigsten die finanziellen Chancen erhalten, die sie verdienen.\n",
    "\n",
    "Fazit: Das Modell ist \"sicherer\" für die Bank - es wird wenig Risiko eingegangen und es wird eher auf Gewinne verzichtet, als Verluste in Kauf zu nehmen. Jedoch liegt eine Diskriminierung nach Geschlecht vor, sowie nach Elternschaft. Die Vorhersagequalität für die benachteiligten Gruppen zu verbessern könnte zu höheren Gewinnen und niedrigeren Verlusten für die Bank führen.\n",
    "\n",
    "b) Challenge: Mach der Bank einen Vorschlag, wie sie versuchen könnte, das Modell zu verbessern.\n",
    "\n",
    "Um diese Frage zu beantworten sollte man Überlegungen anstellen, was Gründe für die Verzerrung sein könnten. Eine häufig betrachtete Ursache liegt in der Verzerrtheit der Trainingsdaten (Garbage-in-Garbage-out). Es könnte z.B. auch hier sein, dass historisch bedingt Väter mit Kinder in den Trainingsdaten besonders oft kreditwürdig sind und Mütter besonders oft kreditunwürdig sind, oder in den Daten weniger vorkommen. Die Bank sollte also ihren Trainingsdatensatz darauf überprüfen, ob aus allen (intersektionalen) Gruppen gleich viele kreditwürdige und nicht-kreditwürdige Personen enthalten sind. \n",
    "Es kann viele weitere Faktoren geben, die Einfluss auf die Verzerrung haben. Eine gute Quelle zu den Möglichen Ursachen für Verzerrung ist [Suresh & Guttag 2021](https://doi.org/10.1145/3465416.3483305). "
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
