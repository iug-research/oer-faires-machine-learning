{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "## About this Notebook\n",
    "\n",
    "This notebook demonstrates a data-based audit of the \"IdentiTOP\" system. IdentiTOP is a fictional software for identifying participants in online exams.\n",
    "\n",
    "This notebook is structured as follows:\n",
    "1. Description of the IdentiTOP software\n",
    "2. Identification of potential risks of IdentiTOP\n",
    "3. Definition of quality metrics to evaluate IdentiTOP's risks\n",
    "4. Calculation of quality metrics for IdentiTOP\n",
    "5. Interpretation of results: What risks does IdentiTOP actually pose?\n",
    "6. Recommendations: How can IdentiTOP be improved?\n",
    "\n",
    "### Libraries Used\n",
    "* This notebook uses the [pandas](https://pandas.pydata.org/docs/reference/index.html) library for data processing.\n",
    "* The [sklearn](https://scikit-learn.org) library is used to compute [quality metrics](https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics).\n",
    "* The [fairlearn](https://fairlearn.org/v0.8/user_guide/) library allows computation of quality metrics per group for the model. This library may need to be installed first, e.g., via `pip install fairlearn` or Anaconda.\n",
    "\n",
    "### Prerequisite\n",
    "This notebook is part of the OER \"Fair Machine Learning.\" It is intended to be used in collaboration with an instructor."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 1. Description of the IdentiTOP Software\n",
    "\n",
    "Online exams carry the risk that students may not take the test themselves, but hire someone else to do it. IdentiTOP aims to prevent this type of fraud in online exams.\n",
    "\n",
    "When a person registers at an educational institution (e.g., a university), they are required to submit a copy of their ID. These ID photos are stored in a database. IdentiTOP has access to the database of all students' ID photos and also knows which courses each student is enrolled in. At the time of registration at the institution and at the beginning of an online exam, test-takers are informed about the use of IdentiTOP and the consequences of cheating.\n",
    "\n",
    "During the exam, IdentiTOP takes random webcam photos of the person taking the test. The system then retrieves the registered ID photo of the person from the database. It inputs both the ID photo and the webcam photo into a machine learning model, which predicts whether the photos depict the same person.\n",
    "\n",
    "If the system determines that the photos show the same person, the individual can continue working undisturbed and will be checked again by IdentiTOP in a few minutes.\n",
    "\n",
    "If, however, the model determines that the photos do not match, the exam attempt is terminated, and the cheating individual receives a failing grade for the attempt.\n",
    "\n",
    "The creators of IdentiTOP promote the system as relieving proctors from the need to continuously monitor potentially hundreds of test-takers during an exam. It also spares proctors from the uncomfortable task of personally intervening in cases of suspected cheating. This ensures consistent detection and penalization of cheating while reducing its occurrence due to informed awareness of the system and its consequences."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 2. Identification of Potential Risks\n",
    "\n",
    "* **(R1)** If IdentiTOP incorrectly concludes that the photos do not depict the same person, it could result in significant emotional distress for the innocent individual, as well as organizational effort (to contest the decision and schedule a new exam attempt) or even academic disadvantages (if a new attempt is not granted).\n",
    "* **(R2)** If IdentiTOP incorrectly concludes that the photos depict the same person, even though they are only similar, the fraud would go undetected, and the individual's true abilities would remain unassessed. This could lead to the person enrolling in an advanced course or getting a job for which they are unqualified, negatively affecting all parties involved depending on the situation.\n",
    "* **(R3)** If IdentiTOP works better for lighter-skinned males than for other demographic groups (see “Gender Shades” [4]), some groups would be disproportionately disadvantaged, reinforcing existing structural discrimination.\n",
    "* **(R4)** If IdentiTOP performs better for individuals with higher-quality webcams, financially disadvantaged groups could face discrimination.\n",
    "* **(R5)** Test-takers might feel that their privacy is violated due to unexpected photos being taken. This could result in emotional distress and negatively impact their exam performance.\n",
    "\n",
    "Additional risk hypotheses are conceivable, such as discrimination against transgender individuals (e.g., if their appearance no longer matches their ID photo). While testing these hypotheses goes beyond the scope of this notebook, comprehensive risk hypothesis generation and testing are, of course, important in practice."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 3. Definition of Quality Metrics to Evaluate the Risks\n",
    "\n",
    "* **(R1 - IdentiTOP incorrectly concludes that the photos do not match)** is acceptable if true positive cases result in few false negatives (FN). This can be measured using the **False Negative Rate (FNR)**. A low FNR reduces the likelihood of R1 occurring.\n",
    "  **FNR = FN / (FN + TP)**\n",
    "\n",
    "* **(R2 - IdentiTOP incorrectly concludes that the photos match)** is acceptable if true negative cases result in few false positives (FP). This can be measured using the **False Positive Rate (FPR)**. A low FPR reduces the likelihood of R2 occurring.\n",
    "  **FPR = FP / (FP + TN)**\n",
    "\n",
    "* **(R3 - IdentiTOP performs better for certain demographic groups)** has two parts:\n",
    "  * Risk **(a)** is acceptable if the quality criterion for (R1) is met equally across genders and skin tones. This corresponds to the fairness definition of **Equal Opportunity** (FNR parity).\n",
    "  * Risk **(b)** is acceptable if the quality criterion for (R2) is met equally across genders and skin tones. This corresponds to the fairness definition of **Predictive Parity** (FPR parity).\n",
    "\n",
    "* **(R4)** corresponds to (R3) but groups are categorized by webcam quality instead. This risk is not separately addressed in this example, as it is similar to (R3). However, it would be essential to evaluate R4 in practice.\n",
    "\n",
    "* **(R5)** is acceptable if most test-takers report feeling unbothered by IdentiTOP's impact on their privacy. To verify this, a survey of IdentiTOP users would need to be conducted and analyzed. While this step is excluded in this example, evaluating R5 would be crucial in real-world applications.\n",
    "\n",
    "**FP** and **TN** just look like a jumble of letters? Review the [Confusion Matrix](https://en.wikipedia.org/wiki/Confusion_matrix)!\n",
    "\n",
    "**Equal Opportunity** and **Predictive Parity** are just two of many possible fairness definitions. Additional definitions are explained by [Verma & Rubin 2018](https://fairware.cs.umass.edu/papers/Verma.pdf)."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 4. Calculation of Quality Metrics\n",
    "\n",
    "IdentiTOP was specifically tested using a balanced selection of test participants. Out of 300 participants, 160 were instructed to \"cheat\" to evaluate how well IdentiTOP functions. The demographic data of the participants were recorded as binary values.\n",
    "\n",
    "The test data is available in a CSV file and is imported in the first step. The dataset contains information about gender, skin color, whether cheating actually occurred (\"truth\"), and whether IdentiTOP detected cheating (\"prediction\").\n",
    "\n",
    "The data is encoded as follows:\n",
    "* **Gender (\"gender\")**: 0 = male, 1 = female\n",
    "* **Skin color (\"skincolor\")**: 0 = light-skinned, 1 = dark-skinned\n",
    "* **Truth (\"truth\")**: 0 = images do not show the same person (negative outcome), 1 = images show the same person (positive outcome)\n",
    "* **Prediction (\"prediction\")**: 0 = images do not show the same person (negative outcome), 1 = images show the same person (positive outcome)\n",
    "\n",
    "In the first step, the data will be imported.\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "d = pd.read_csv(\"identitop-prediction-results.csv\") # import the CSV\n",
    "\n",
    "d.head() # show a few rows of the imported data set"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### (R1) and (R2) - General Risks\n",
    "\n",
    "For **(R1)** and **(R2)**, the [Confusion Matrix](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html#sklearn.metrics.confusion_matrix) is calculated and visualized as a first step."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay\n",
    "\n",
    "tn, fp, fn, tp = confusion_matrix(d[\"truth\"], d[\"prediction\"]).ravel() # Get the confusion matrix.\n",
    "ConfusionMatrixDisplay.from_predictions(d[\"truth\"], d[\"prediction\"], display_labels=['not the same person', 'the same person'], cmap='binary')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The truth matrix of IdentiTOP indicates the following:\n",
    "\n",
    "* **113 times correctly predicted** that the images show the same person.\n",
    "* **10 times incorrectly predicted** that the images show the same person.\n",
    "* **27 times incorrectly predicted** that the images do not show the same person.\n",
    "* **150 times correctly predicted** that the images do not show the same person.\n",
    "\n",
    "This means the model made errors in **37 out of 300 cases**, while it was correct in the remaining **263 cases**.\n",
    "\n",
    "Using the confusion matrix, the values for **True Negatives (TN)**, **False Positives (FP)**, etc., are substituted into the formulas for **False Positive Rate (FPR)** and **False Negative Rate (FNR)** to calculate the metrics."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "fnr = fn / (tp + fn) # Calculate FNR\n",
    "print('(R1) FNR: {}'.format(fnr))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "fpr = fp / (tn + fp) # Calculate FPR\n",
    "print('(R2) FPR: {}'.format(fpr))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### (R3) - Quality per Group / Discrimination Risks\n",
    "\n",
    "The equality of quality across groups can be assessed using either the ratio or the difference between the groups.\n",
    "\n",
    "#### How Equal Should the Quality Be?\n",
    "\n",
    "This is not strictly defined. Stakeholders must decide whether the observed difference is too large. For guidance, they may refer to the commonly used \"[Disparate Impact](https://en.wikipedia.org/wiki/Disparate_impact)\" or \"80%\" rule, which states that:\n",
    "\n",
    "* The **ratio** should be > 0.8 and < 1.2.\n",
    "* The **difference** should be < 0.2."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R3 a - gender\n",
    "from fairlearn.metrics import MetricFrame, false_negative_rate, false_positive_rate\n",
    "\n",
    "print(\"Equal Opportunity for Women\")\n",
    "print(\"\\n\")\n",
    "\n",
    "fnrgend = MetricFrame(metrics={\"FNR\": false_negative_rate},\n",
    "                y_true=d[\"truth\"],\n",
    "                y_pred=d[\"prediction\"],\n",
    "                sensitive_features=d[\"gender\"]).by_group\n",
    "\n",
    "fnr_mas = fnrgend[\"FNR\"][0]\n",
    "fnr_fem = fnrgend[\"FNR\"][1]\n",
    "\n",
    "print(fnrgend)\n",
    "print(\"\\n\")\n",
    "print(\"FNR-Ratio: {} / {} = {}\".format(fnr_fem, fnr_mas, fnr_fem/fnr_mas))\n",
    "print(\"FNR-Difference: {} - {} = {}\".format(fnr_fem, fnr_mas,  fnr_fem - fnr_mas))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R3 a - skincolor\n",
    "print(\"Equal Opportunity for people with darker skin\")\n",
    "print(\"\\n\")\n",
    "\n",
    "fnrskin = MetricFrame(metrics={\n",
    "    \"FNR\": false_negative_rate},\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[\"skincolor\"]).by_group\n",
    "\n",
    "fnr_lighterskin = fnrskin[\"FNR\"][0]\n",
    "fnr_darkerskin = fnrskin[\"FNR\"][1]\n",
    "\n",
    "print(fnrskin)\n",
    "print(\"\\n\")\n",
    "print(\"FNR-Ratio: {} / {} = {}\".format(fnr_darkerskin, fnr_lighterskin, fnr_darkerskin / fnr_lighterskin))\n",
    "print(\"FNR-Difference: {} - {} = {}\".format(fnr_darkerskin, fnr_lighterskin, fnr_darkerskin - fnr_lighterskin))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R3 a - gender and skincolor\n",
    "print(\"Intersectional Equal Opportunity\")\n",
    "print(\"\\n\")\n",
    "\n",
    "fnrgendskin = MetricFrame(metrics={\n",
    "    \"FNR\": false_negative_rate},\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[[\"gender\", \"skincolor\"]]).by_group\n",
    "\n",
    "print(fnrgendskin)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Ratio of the group's FNR to the minimum FNR\")\n",
    "print(fnrgendskin / fnrgendskin[\"FNR\"].min())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Difference of the group's FNR to the minimum FNR\")\n",
    "print(fnrgendskin - fnrgendskin[\"FNR\"].min())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Ratio of the group's FNR to the mean FNR\")\n",
    "print(fnrgendskin / fnrgendskin[\"FNR\"].mean())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Difference of the group's FNR to the mean FNR\")\n",
    "fnrdiff = fnrgendskin - fnrgendskin[\"FNR\"].mean()\n",
    "print(fnrdiff)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R3 b - gender\n",
    "print(\"Predictive Equality for Women\")\n",
    "print(\"\\n\")\n",
    "\n",
    "fprgend = MetricFrame(metrics={\n",
    "    \"FPR\": false_positive_rate},\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[\"gender\"]).by_group\n",
    "\n",
    "fpr_mas = fprgend[\"FPR\"][0]\n",
    "fpr_fem = fprgend[\"FPR\"][1]\n",
    "\n",
    "print(fprgend)\n",
    "print(\"\\n\")\n",
    "print(\"FPR-Ratio: {} / {} = {}\".format(fpr_fem, fpr_mas, fpr_fem/fpr_mas))\n",
    "print(\"FPR-Difference: {} - {} = {}\".format(fpr_fem, fpr_mas, fpr_fem - fpr_mas))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R3 b - skincolor\n",
    "print(\"Predictive Equality for people with darker skin\")\n",
    "print(\"\\n\")\n",
    "\n",
    "fprskin = MetricFrame(metrics={\n",
    "    \"FPR\": false_positive_rate},\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[\"skincolor\"]).by_group\n",
    "\n",
    "fpr_lighterskin = fprskin[\"FPR\"][0]\n",
    "fpr_darkerskin = fprskin[\"FPR\"][1]\n",
    "\n",
    "print(fprskin)\n",
    "print(\"\\n\")\n",
    "print(\"FPR-Ratio: {} / {} = {}\".format(fpr_darkerskin, fpr_lighterskin, fpr_darkerskin/fpr_lighterskin))\n",
    "print(\"FPR-Difference: {} - {} = {}\".format(fpr_darkerskin, fpr_lighterskin, fpr_darkerskin - fpr_lighterskin))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# R3 b - gender and skincolor\n",
    "print(\"Intersectional Predictive Equality\")\n",
    "print(\"\\n\")\n",
    "\n",
    "fprgendskin = MetricFrame(metrics={\n",
    "    \"FPR\": false_positive_rate},\n",
    "    y_true=d[\"truth\"],\n",
    "    y_pred=d[\"prediction\"],\n",
    "    sensitive_features=d[[\"gender\", \"skincolor\"]]).by_group\n",
    "\n",
    "print(fprgendskin)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Ratio of the group's FPR to the minimum FPR\")\n",
    "print(fprgendskin / fprgendskin[\"FPR\"].min())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Difference of the group's FPR to the minimum FPR\")\n",
    "print(fprgendskin - fprgendskin[\"FPR\"].min())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Ratio of the group's FPR to the mean FPR\")\n",
    "print(fprgendskin / fprgendskin[\"FPR\"].mean())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "print(\"Difference of the group's FPR to the mean FPR\")\n",
    "print(fprgendskin - fprgendskin[\"FPR\"].mean())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 5. Interpretation of Results: What Risks Does the Software Actually Pose?\n",
    "\n",
    "* **(R1)** Of all positive cases (where the person did not cheat), **20% were falsely flagged as cheating**. This means that in one out of five alarms, an innocent person is interrupted during their exam attempt. This risk hypothesis is confirmed.\n",
    "* **(R2)** **6% of cheating test-takers went undetected by the model.** This indicates that the risk of test-takers fraudulently earning credentials unnoticed is relatively low and thus acceptable.\n",
    "* **(R3 a)** Among non-cheating individuals, **dark-skinned individuals, especially dark-skinned women, are falsely flagged as cheating more often than light-skinned individuals.** Dark-skinned women are flagged twice as often as dark-skinned men. Light-skinned women are the least likely to receive a false alarm, and light-skinned men only slightly more so. This shows no general gender discrimination but significant discrimination based on skin color, particularly affecting dark-skinned women.\n",
    "* **(R3 b)** While the **FPR** is relatively low for all groups, the model fails to detect cheating slightly more often for dark-skinned individuals than for light-skinned ones. Among cheaters, light-skinned women are most frequently detected, followed by dark-skinned men, then light-skinned men, and finally dark-skinned women, whose cheating goes undetected in **11% of cases**.\n",
    "\n",
    "### Conclusion\n",
    "IdentiTOP generally detects cheating attempts when they occur. However:\n",
    "- **Dark-skinned cheaters, particularly dark-skinned women, are more often undetected** compared to light-skinned cheaters.\n",
    "- A **non-negligible proportion of innocent test-takers are falsely interrupted**, causing undue burden.\n",
    "- Among dark-skinned individuals—and particularly dark-skinned women—**false alarms occur significantly more frequently** than among light-skinned individuals.\n",
    "Thus, IdentiTOP disproportionately disadvantages dark-skinned individuals, especially dark-skinned women."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 6. Recommendations: How Can the Software Be Improved?\n",
    "\n",
    "1. **Reduce the burden on innocent test-takers** by implementing a \"Human-in-the-Loop\" strategy. In this approach, a human operator reviews the photos in cases of a cheating alarm and only approves exam termination if the human confirms the cheating attempt.\n",
    "\n",
    "2. **Improve the model's quality** to better align with one of IdentiTOP's original goals: reducing the workload for proctors.\n",
    "   - The high **FNR** and the **inequality in FNR and, to some extent, FPR** may stem from issues with IdentiTOP's training data.\n",
    "   - To address this, IdentiTOP should be retrained using a dataset that includes more dark-skinned individuals, particularly dark-skinned women, and then tested again."
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}